#!/bin/bash

black --target-version py37 --skip-string-normalization --line-length 110 .

isort --profile black --line-length 110 .
