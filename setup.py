#! /usr/bin/env python
#
'''
   Setup script for PII Manager proof-of-concept implementations
'''

import os
import subprocess

from setuptools import find_packages, setup


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


setup(
    name="pii-manager-poc",
    version=get_version(),
    license="AGPLv3+",
    description="PII Manager proof-of-concept implementation",
    url="http://git.entrouvert.org/pii-manager-poc.git/",
    author="Entr'ouvert",
    author_email="pmarillonnet@entrouvert.com",
    maintainer="Paul Marillonnet",
    maintainer_email="pmarillonnet@entrouvert.com",
    scripts=('manage.py',),
    packages=find_packages('manager'),
    package_dir={
        '': 'manager',
    },
    include_package_data=True,
    install_requires=[
        'django>=1.11,<2.3',
        'requests>=2.3',
        'requests-oauthlib',
        'Django-Select2>5,<6',
        'django-ratelimit',
        'djangorestframework>=3.3,<3.10',
        'pycryptodomex',
        'django-mellon>=1.22',
        'jwcrypto>=0.3.1,<1',
        'cryptography',
        'XStatic-jQuery<2',
        'XStatic-jquery-ui',
        'xstatic-select2',
        'pytz',
        'sqlparse',
        'django-phonenumber-field',
        'phonenumbers',
    ],
    zip_safe=False,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Framework :: Django",
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Information Technology',
        'Intended Audience :: Legal Industry',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Telecommunications Industry',
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: System :: Systems Administration :: Authentication/Directory",
    ],
    cmdclass={},
)
