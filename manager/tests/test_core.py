import pytest
from httmock import HTTMock, urlmatch

from manager.core.models import (
    ConsentReceipt,
    OAuthServer,
    OAuthSource,
    PiiCategory,
    PiiController,
    Purpose,
    Service,
)

pytestmark = pytest.mark.django_db


@pytest.fixture
def oauth_source():
    # todo mock responses
    return OAuthSource.object.create(name='test oauth source', url='https://www.myoauthsource.org/resources/')


def oauth_source_mock(oauth_source):
    # @urlmatch todo
    def resource_endpoint_mock():
        pass

    return HTTMock()  # todo


@pytest.fixture
def oauth_server():
    # todo mock responses
    return OAuthServer.objects.create(
        name='test oauth server', url='https://www.myoauthserver.org/authorize/'
    )


def oauth_server_mock(oauth_server):
    # @urlmatch todo
    def authz_endpoint_mock():
        pass

    # @urlmatch todo
    def token_endpoint_mock():
        pass

    return HTTMock()  # todo


@pytest.fixture
def oauth_urm_client():
    pass


def test_simple_oauth(oauth_source, oauth_server):
    with oauth_source_mock(oauth_source):
        with oauth_server_mock(oauth_server):
            pass
