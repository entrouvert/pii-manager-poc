# Generated by Django 2.2.20 on 2021-04-29 13:40

import django.db.models.deletion
import phonenumber_field.modelfields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('street_number', models.CharField(max_length=31, verbose_name='street number')),
                ('street_name', models.CharField(max_length=255, verbose_name='street name')),
                ('zip_code', models.CharField(max_length=15, verbose_name='zip code')),
                ('city', models.CharField(max_length=127, verbose_name='city')),
                ('region', models.CharField(max_length=127, verbose_name='region')),
                ('country', models.CharField(max_length=127, verbose_name='country')),
            ],
        ),
        migrations.CreateModel(
            name='AuthorizationCode',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('value', models.CharField(max_length=255, verbose_name='value of the authorization code')),
                ('expires', models.DateTimeField(verbose_name='authorization code expiry timestamp')),
            ],
        ),
        migrations.CreateModel(
            name='KerberosSource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=127, verbose_name='source name')),
                (
                    'source_type',
                    models.CharField(
                        choices=[
                            ('oauth', 'OAuth'),
                            ('saml', 'SAML'),
                            ('rest', 'REST'),
                            ('kerberos', 'Kerberos'),
                        ],
                        default='oauth',
                        max_length=15,
                        verbose_name='source type',
                    ),
                ),
                ('url', models.URLField(unique=True, verbose_name='source url')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OAuthServer',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=127, verbose_name='provider name')),
                ('url', models.URLField(unique=True, verbose_name='provider url')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OAuthSource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=127, verbose_name='source name')),
                (
                    'source_type',
                    models.CharField(
                        choices=[
                            ('oauth', 'OAuth'),
                            ('saml', 'SAML'),
                            ('rest', 'REST'),
                            ('kerberos', 'Kerberos'),
                        ],
                        default='oauth',
                        max_length=15,
                        verbose_name='source type',
                    ),
                ),
                ('url', models.URLField(unique=True, verbose_name='source url')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PiiCategory',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'name',
                    models.CharField(max_length=127, unique=True, verbose_name='target category of pii'),
                ),
                ('is_sensitive', models.BooleanField(default=False, verbose_name='sensitivity of pii')),
            ],
        ),
        migrations.CreateModel(
            name='Purpose',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'description',
                    models.CharField(
                        max_length=255, verbose_name='short description of the pii collection purpose'
                    ),
                ),
                (
                    'category',
                    models.CharField(
                        choices=[
                            ('core function', 'core function'),
                            ('contracted service', 'contracted service'),
                            ('delivery', 'delivery'),
                            ('contact requested', 'contact requested'),
                            ('personalized experience', 'personalized experience'),
                            ('marketing', 'marketing'),
                        ],
                        default='core function',
                        max_length=127,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name='RestSource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=127, verbose_name='source name')),
                (
                    'source_type',
                    models.CharField(
                        choices=[
                            ('oauth', 'OAuth'),
                            ('saml', 'SAML'),
                            ('rest', 'REST'),
                            ('kerberos', 'Kerberos'),
                        ],
                        default='oauth',
                        max_length=15,
                        verbose_name='source type',
                    ),
                ),
                ('url', models.URLField(unique=True, verbose_name='source url')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('name', models.CharField(max_length=255, verbose_name='name of the service')),
                (
                    'client_id',
                    models.CharField(max_length=255, verbose_name='client identifier of the service'),
                ),
                (
                    'client_secret',
                    models.CharField(max_length=255, verbose_name='client secret of the service'),
                ),
            ],
        ),
        migrations.CreateModel(
            name='TerminationPolicy',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'description',
                    models.CharField(max_length=255, verbose_name='conditions for termination of consent'),
                ),
            ],
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('scopes', models.CharField(max_length=255, verbose_name='scopes for the access token')),
                ('expires', models.DateTimeField(verbose_name='expiration timestamp')),
                ('audience', models.CharField(max_length=255, verbose_name='audience of the token')),
                ('not_before', models.DateTimeField(verbose_name='token not usable before')),
                (
                    'code',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='core.AuthorizationCode',
                        verbose_name='access token',
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name='PiiController',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'public_key',
                    models.CharField(blank=True, max_length=8191, null=True, verbose_name='public key'),
                ),
                (
                    'contact',
                    models.CharField(max_length=255, verbose_name='contact name of the pii controller'),
                ),
                (
                    'email',
                    models.EmailField(max_length=254, verbose_name='pii controller contact email address'),
                ),
                (
                    'phone_number',
                    phonenumber_field.modelfields.PhoneNumberField(
                        max_length=128, region=None, verbose_name='pii controller contact phone number'
                    ),
                ),
                (
                    'contact_url',
                    models.URLField(blank=True, null=True, verbose_name='pii controller contact url'),
                ),
                (
                    'privacy_policy_url',
                    models.URLField(verbose_name="link to the pii controller's privacy statement"),
                ),
                (
                    'address',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='core.Address',
                        verbose_name='address of the pii controller',
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name='ConsentReceipt',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('version', models.CharField(max_length=31, verbose_name='receipt version')),
                (
                    'jurisdiction',
                    models.CharField(max_length=255, verbose_name='jurisdiction applying for the receipt'),
                ),
                (
                    'consent_timestamp',
                    models.DateTimeField(auto_now_add=True, verbose_name='consent timestamp'),
                ),
                ('collection_method', models.CharField(max_length=127, verbose_name='collection method')),
                (
                    'receipt_id',
                    models.CharField(max_length=255, verbose_name='uuid4 identifier of the consent receipt'),
                ),
                (
                    'language',
                    models.CharField(
                        blank=True,
                        max_length=63,
                        null=True,
                        verbose_name='language in which the consent was obtained',
                    ),
                ),
                (
                    'pii_principal_id',
                    models.CharField(max_length=255, verbose_name='pii principal-provided identifier'),
                ),
                (
                    'on_behalf',
                    models.BooleanField(
                        default=False,
                        verbose_name='a pii processor acting on behalf of another processor or controller',
                    ),
                ),
                (
                    'consent_type',
                    models.CharField(
                        choices=[('implicit', 'implicit'), ('implicit', 'explicit')],
                        default='explicit',
                        max_length=15,
                    ),
                ),
                (
                    'third_party_disclosure',
                    models.BooleanField(
                        default=False, verbose_name='the pii controller is disclosing pii to a third party'
                    ),
                ),
                (
                    'third_party_name',
                    models.CharField(
                        max_length=255,
                        verbose_name='name or names of the third party to which the controller may disclose the pii',
                    ),
                ),
                (
                    'pii_categories',
                    models.ManyToManyField(related_name='consent_receipts', to='core.PiiCategory'),
                ),
                (
                    'pii_controller',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='core.PiiController',
                        verbose_name='fk to the first pii controller that collects the data',
                    ),
                ),
                (
                    'pii_controllers',
                    models.ManyToManyField(
                        blank=True, related_name='consent_receipts', to='core.PiiController'
                    ),
                ),
                (
                    'purpose',
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to='core.Purpose',
                        verbose_name='purpose of collection',
                    ),
                ),
                ('purposes', models.ManyToManyField(related_name='consent_receipts', to='core.Purpose')),
                (
                    'service',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='core.Service',
                        verbose_name='service or group of services for which PII is collected',
                    ),
                ),
                ('services', models.ManyToManyField(related_name='consent_receipts', to='core.Service')),
                (
                    'termination_policy',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to='core.TerminationPolicy',
                        verbose_name='termination policy',
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name='authorizationcode',
            name='service',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='core.Service',
                verbose_name='authorization code for the service',
            ),
        ),
    ]
