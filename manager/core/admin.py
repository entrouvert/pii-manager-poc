# pii manager - proof-of-concept implementation
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin

from manager.core import models


class OAuthSourceAdmin(admin.ModelAdmin):
    list_display = ['name', 'url']
    list_filter = ['name']


class RestSourceAdmin(admin.ModelAdmin):
    list_display = ['name', 'url']
    list_filter = ['name']


class KerberosSourceAdmin(admin.ModelAdmin):
    list_display = ['name', 'url']
    list_filter = ['name']


class AddressAdmin(admin.ModelAdmin):
    list_display = ['street_number', 'street_name', 'zip_code', 'city', 'region', 'country']


class PiiControllerAdmin(admin.ModelAdmin):
    list_display = [
        'public_key',
        'contact',
        'address',
        'email',
        'phone_number',
        'contact_uri',
        'privacy_policy_url',
    ]
    list_filter = ['contact']


admin.site.register(models.OAuthSource, OAuthSourceAdmin)
admin.site.register(models.RestSource, RestSourceAdmin)
admin.site.register(models.KerberosSource, KerberosSourceAdmin)
admin.site.register(models.Address, AddressAdmin)
admin.site.register(models.PiiController, PiiControllerAdmin)
