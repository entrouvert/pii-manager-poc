# pii manager - proof-of-concept implementation
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import include, url
from django.contrib import admin

from manager.core import views

# from manager.core.api_views import router # todo

urlpatterns = [
    url(r'^resource/(?P<resource_id>[A-Za-z0-9_ -]+)/$', views.resource, name='resource_endpoint'),
    # url(r'^api/', include(router.urls)), # todo
    # url(r'^api/', include('manager.core.api_urls')), # todo
]
