# pii manager - proof-of-concept implementation
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import uuid

from manager.core.exceptions import AuthzError
from manager.core.models import AuthorizationCode, Token


def generate_uuid(value=None):
    return str(uuid.uuid4())


def is_token_valid(authz_code, token):
    try:
        if not isinstance(authz_code, AuthorizationCode):
            raise AuthzError('code must be an AuthorizationCode object')
        if not isinstance(token, (str, Token)):
            raise AuthzError('token must either be str or Token object')
        if isinstance(token, str):
            token = Token.deserialize(token)
        # todo check scopes
        # todo check audience
        # todo check expiry and not_before
        # todo check code
    except AuthzError:
        return False
