# pii manager - proof-of-concept implementation
# Copyright (C) 2021 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class BaseBackend(object):
    '''
    Base class for the PII Manager's Source Backend
    '''

    interactive = False

    def __init__(self, source, *args, **kwargs):
        self.source = source

    def get_resource(self, *args, **kwargs):
        pass


class OAuthBackend(BaseBackend):
    '''
    Minimalistic OAuth 2.0 client for the PII Manager's Source Backend
    Supported grant types are:
    - authorization code (https://tools.ietf.org/html/rfc6749#section-4.1)
    - implicit (https://tools.ietf.org/html/rfc6749#section-4.2)
    '''

    # user interactions are required
    interactive = True

    def __init__(self, source, *args, **kwargs):
        super().__init__(source, *args, **kwargs)
        self.get_authorization()

    def get_authorization(self):
        # required in authorization code grant only
        pass

    def get_token(self, *args, **kwargs):
        pass


class RestBackend(BaseBackend):
    '''
    REST support class for the PII Manager's Source Backend
    '''

    def __init__(self, source, *args, **kwargs):
        super().__init__(source, *args, **kwargs)
        self.authenticate()

    def authenticate(self, *args, **kwargs):
        pass

    def get_credentials(self, *args, **kwargs):
        pass


class SamlBackend(BaseBackend):
    '''
    TODO Not supported yet
    '''

    # user interactions are required, no backchannel pii retrieval
    interactive = True


class KerberosBackend(BaseBackend):
    '''
    TODO Not supported yet
    '''

    pass
