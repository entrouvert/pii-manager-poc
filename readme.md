h1. Quickstart:

On Debian stable/testing, install within a python3 virtual environment:

<pre>
sudo apt install virtualenv python3-xstatic
mkdir ~/pii-manager-venv/
virtualenv -p python3 ~/pii-manager-venv/
source ~/pii-manager-venv/bin/activate
python3 ./setup.py develop
</pre>

h1. Local testing:

<pre>
python3 ./manage.py makemigrations
python3 ./manage.py migrate
python3 ./manage.py runserver 127.0.0.1:8080
</pre>

h1. Todolist:

* PostgreSQL integration
* uwsgi/nginx configuration
* gadjo support
* crud api views on main models

h1. Optional todos:

* hobo integration (?) -- see https://git.entrouvert.org/hobo.git/
* publik devinst integration (?) -- see https://git.entrouvert.org/publik-devinst.git/
